import string
STRING_INDEX = [alpha for alpha in string.ascii_lowercase]

ENCRYPTED_PASSWORD = "cfcf6i6m"
encrypted_password_by_index = [alpha for alpha in ENCRYPTED_PASSWORD]

output = []

for i, char in enumerate(encrypted_password_by_index):
    if char.isalpha():
        output.append(
            STRING_INDEX[
                STRING_INDEX.index(char) - i
            ]
        )
    elif char.isdigit():
        output.append(str(int(char) - i))

print("".join(output))
